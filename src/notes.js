const fs = require('fs')
 

const getNotes = () => {
    return 'Your notes...'
}

const addNote = (name,email,message,date) => {
    const notes = loadNotes() 
   
        notes.push({            
            email: email,
            name: name,
            message:message,
            date:date
        })
        saveNotes(notes)
        
}

const removeNote = (date) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter((note) => note.date !== date)

    if (notes.length > notesToKeep.length) {
        console.log('Note removed!')
        saveNotes(notesToKeep)
    } else {
        console.log('No note found!')
    }    
}

const listNotes = () => {
    const notes = loadNotes()
    return notes;     
}

const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJSON)
}

const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    } catch (e) {
        return []
    }
}

module.exports = {
    getNotes: getNotes,
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes
}