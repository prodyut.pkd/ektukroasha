const path = require('path')
const express = require('express')
 const hbs = require('hbs')

 
const jsdom = require("jsdom");
const fs = require('fs');
var moment = require('moment');

const app = express()
var cors = require('cors')
 
app.use(cors())

const notes = require('./notes.js') 
 

const port = process.env.PORT || 3000

// // Define paths for Express config
const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

// // Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// // Setup static directory to serve
app.use(express.static(publicDirectoryPath))

app.get('', (req, res) => {
    res.render('index', {
        title: 'Home' 
    }); 
     
})

   
app.get('/management', (req, res) => {
    res.render('management', {
        title: 'About Me',
        name: 'Andrew Mead'
    })
})
app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About Me' 
    })
})

app.get('/contact', (req, res) => {    
    res.render('contact', {        
        title: 'Contact',
        success: false       
    })
})

 

 // Parse URL-encoded bodies (as sent by HTML forms)
app.use(express.urlencoded());

// Parse JSON bodies (as sent by API clients)
app.use(express.json());

// Access the parse results as request.body
app.post('/contact', function(request, response){
    var name = request.body.user.name;
    var email=request.body.user.email;
    var message=request.body.user.message;
    var date = moment().format('Y-M-D H:m:s');
    notes.addNote(name,email,message,date);
     
      response.render('contact', { success: true });
}); 

  
app.get('/admin', (req, res) => {
    res.render('admin', {
        title: 'admin' ,
        success: false       
    }); 
 
})
app.post('/admin', (request, res) => {
    var password = request.body.password;
      
if (password=='ka09092019') {    
    var noteList = notes.listNotes();
    
    res.render('admin', {  
        title: 'admin' ,
        success: false  ,
        noteList: noteList     
    })
} 
else{    
    res.render('admin', {  
        title: 'admin' ,
        success: true       
    })
}
})
 
app.post('/messageDelete', (request, res) => {
    var chkDate = request.body.chkDate;
    notes.removeNote(chkDate)     
    var noteList = notes.listNotes();
    res.render('admin', {  
        title: 'admin' ,
        success: false  ,
        noteList: noteList     
    })
 
})

 

app.get('/June', (req, res) => {
    res.render('June', {  
        title: 'SiteVisit' 
    })
})
  
 
app.listen(port, () => {
    console.log('Server is up on port '+port)
})